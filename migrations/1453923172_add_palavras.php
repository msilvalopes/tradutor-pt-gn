<?php

class AddPalavras extends Migration {
	// public $connection = 'default';
	public $auditar = false;
	public $up = [
		'new_table' => [
			'palavras' => [
				'idioma_id' => ['int'],
				'classificacao_palavra_id' => ['int'],
				'palavra' => ['string', 'size' => 100],
				'significado' => ['string', 'size' => 1000],
				'explicacao' => ['text'],
				'criador_id' => ['int'],
				'ultimo_modificador_id' => ['int'],
				'data_criacao' => ['datetime'],
				'data_modificacao' => ['datetime'],
				'sufixo' => ['int'],
				'prefixo' => ['int'],
				'ativo' => ['int'],
				'validada' => ['int'],
			],
		],
	];
	public $down = [
		'del_table' => [
			'palavras',
		],
	];
	// $dir is "up" or "down"
	public function before($dir) {

	}

	// $dir is "up" or "down"
	public function after($direction) {

	}
}
