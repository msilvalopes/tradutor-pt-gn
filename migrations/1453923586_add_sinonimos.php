<?php

class AddSinonimos extends Migration {
	// public $connection = 'default';
	public $auditar = false;
	public $up = [
		'new_table' => [
			'sinonimos' => [
				'palavra_id' => ['int'],
				'sinonimo_id' => ['int'],
				'ordem' => ['int'],
				'eh_antonimo' => ['int'],
			],
		],
	];
	public $down = [
		'del_table' => [
			'sinonimos',
		],
	];
	// $dir is "up" or "down"
	public function before($dir) {

	}

	// $dir is "up" or "down"
	public function after($direction) {

	}
}
