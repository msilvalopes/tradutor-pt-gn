<?php

class AddClassificacaoPalavras extends Migration {
	// public $connection = 'default';
	public $auditar = false;
	public $up = [
		'new_table' => [
			'classificacao_palavras' => [
				'sigla' => ['string', 'size' => 10],
				'titulo' => ['string', 'size' => 10],
				'explicacao' => ['text'],
			],
		],
	];
	public $down = [
		'del_table' => [
			'classificacao_palavras',
		],
	];
	// $dir is "up" or "down"
	public function before($dir) {

	}

	// $dir is "up" or "down"
	public function after($direction) {

	}
}
