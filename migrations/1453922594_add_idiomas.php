<?php

class AddIdiomas extends Migration {
	// public $connection = 'default';
	public $auditar = false;
	public $up = [
		'new_table' => [
			'idiomas' => [
				'idioma' => ['string', 'size' => 100],
				'resumo' => ['text'],
				'descricao' => ['text'],

			],
		],
	];

	public $down = [
		'del_table' => [
			'idiomas',
		],
	];
	// $dir is "up" or "down"
	public function before($dir) {

	}

	// $dir is "up" or "down"
	public function after($direction) {

	}
}
