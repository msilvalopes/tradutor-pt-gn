<?php

class AddPalavrasHistorico extends Migration {
	// public $connection = 'default';
	public $auditar = false;
	public $up = [
		'new_table' => [
			'palavras_historico' => [
				'_config' => ['id' => ['bigint', 'serial']],
				'palavra_id' => ['int'],
				'usuario_id' => ['int'],
				'significado' => ['string', 'size' => 1000],
				'explicacao' => ['text'],
				'data_operacao' => ['datetime'],
			],
		],
	];
	public $down = [
		'del_table' => [
			'palavras_historico',
		],
	];
	// $dir is "up" or "down"
	public function before($dir) {

	}

	// $dir is "up" or "down"
	public function after($direction) {

	}
}
