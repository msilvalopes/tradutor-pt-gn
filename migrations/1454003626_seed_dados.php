<?php

class SeedDados extends Migration {
    // public $connection = 'default';
    public $auditar = false;
    public $up = [
        /*'trucate_table' => [
            'classificacao_palavras'
        ],
        'reset_serial' => [
            'classificacao_palavras.id'
        ],*/
        'seed_by_json' => [
            'classificacao_palavras' => 'seeds/classificacao_palavras.json',
            'idiomas' => 'seeds/idiomas.json',
            
        ]
    ];
    public $down = [
        'trucate_table' => [
            'classificacao_palavras'
        ],
        'reset_serial' => [
            'classificacao_palavras.id'
        ]
    ];
    // $dir is "up" or "down"
    public function before($dir)
    {
        		
    }

    // $dir is "up" or "down"
    public function after($direction)
    {
        		
    }
}
