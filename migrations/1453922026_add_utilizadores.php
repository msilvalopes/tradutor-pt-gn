<?php

class AddUtilizadores extends Migration {
	// public $connection = 'default';
	public $auditar = false;
	public $up = [
		'new_table' => [
			'utilizadores' => [
				'login' => ['string', 'size' => 100],
				'email' => ['string', 'size' => 100],
				'senha' => ['string', 'size' => 64],
				'ativo' => ['int'],
				'pontos' => ['int'],
			],
		],
	];
	public $down = [
		'del_table' => [
			'utilizadores',
		],
	];
	// $dir is "up" or "down"
	public function before($dir) {

	}

	// $dir is "up" or "down"
	public function after($direction) {

	}
}
