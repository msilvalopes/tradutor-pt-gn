<div class="home-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
		        <div class="row">
		            <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3 text-center">
	                	<img src="/images/logo_dictionary.svg" alt="" class="img-responsive">
	                </div>
                </div>
	            <h1>Dicionário Tupi-Guarani &amp; Português</h1>
	            <form method="get" action="/traduzir">
			        <div class="row">
			            <div class="col-sm-12">
			                <?=GenericAdmin::loadForm('index_tradutor');?> 
			            </div>
			            <div class="col-md-12 text-right">
	                		<input type="submit" class="btn btn-info btn-tradutor" value="Traduzir">
	                	</div>
                	</div>
                </form>
        	</div>
        </div>
    </div>
</div>