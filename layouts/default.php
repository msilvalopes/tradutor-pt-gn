<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $titulo ?></title>
	<meta name="description" content="<?=configure::get('site_description')?>">
	<meta http-equiv="X-UA-Compatible" content="IE=EDGE">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<?php foreach (View::getJsList() as $file) { ?>
	<script type="text/javascript" src="<?php echo $file?>"></script>
	<?php } ?>
	<?php foreach (View::getCssList() as $file) { ?>
	<link rel="stylesheet" type="text/css" href="<?php echo $file?>">
	<?php } ?>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only"><?= $titulo ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="javascript:void(0)"><?= $titulo ?></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?=$action == 'index' ? 'active' : ''?>">
                        <a href="/"><?=_('Home')?> <span class="sr-only">(atual)</span></a>
                    </li>
                    <li class="<?=$action == 'de_portugues' ? 'active' : ''?>">
                        <a href="/dicionario/portugues"><?=_('Português')?></a>
                    </li>
                    <li class="<?=$action == 'de_tupi_guarani' ? 'active' : ''?>">
                        <a href="/dicionario/tupi-guarani"><?=_('Tupi-guarani')?></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">
                            <?=_('Dicionários')?> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/tupi-guarani"><?=_('Tupi-guarani')?></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                            aria-expanded="false">
                            <?=_('Gestão')?> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/idiomas"><?=_('Idiomas')?></a></li>
                            <li><a href="/palavras"><?=_('Palavras')?></a></li>
                            <li><a href="/classes_gramaticais"><?=_('Classes gramaticais')?></a></li>
                        </ul>
                    </li>
                    <li class=""><a href="/sobre"><?=_('Sobre')?></a></li>
                </ul>
            </div>
        </div>
    </nav>
	<?=View::drawMessages().$conteudo_para_layout?>
    <footer>
    </footer>
</body>
</html>
