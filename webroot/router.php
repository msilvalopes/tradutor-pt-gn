<?php
define('_ROOT_', realpath($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR);

define('DEV', $_SERVER['SERVER_ADMIN'] == 'marcel');
if(DEV)
{
	ini_set('display_errors', 'On');
}
else {
	ini_set('display_errors', 'Off');
}
define('_MAW_BASE_', realpath(_ROOT_.DIRECTORY_SEPARATOR.'MAW_FW').DIRECTORY_SEPARATOR);

require _MAW_BASE_.'router.php';
die;
