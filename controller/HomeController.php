<?php

class HomeController extends AppController
{
	public $titles = [
		'index' => 'Tradutor Português - Guarani',
	];
	public function afterFilter() {

	}

	public function beforeFilter() {
		$this->set([
			'titulo' => empty($this->titles[$this->action]) ? $this->titles['index'] : $this->titles[$this->action],
			'action' => $this->action
		]);
		View::addJs('js/jquery-2.1.1.min', 'js/bootstrap.min');
		View::addCss(
			'css/bootstrap.min', 'css/dicionario', 'css/font-awesome'
		);
	}

	public function index() {
	}
	public function de_portugues() {
	}
	public function traduzir($fonte = 1, $palavra = null) {
		@$fonte = (int) ($fonte ?: get('fonte'));
		@$palavra = (string) ($palavra ?: get('palavra'));
		if (!in_array($fonte, [1, 2, 3])) {
			Url::redirect('/');
		}
		$params = [$palavra];
		if ($fonte == 1) {
			$query = 'SELECT P.*, TP.tipo_palavra
			FROM palavras P
			LEFT JOIN tipo_palavras TP ON P.tipo_palavra_id = TP.id
			WHERE P.verbete LIKE ?';
		}
		else {
			$query = 'SELECT P.*, TP.tipo_palavra
			FROM palavras P
			LEFT JOIN tipo_palavras TP ON P.tipo_palavra_id = TP.id
			WHERE P.verbete LIKE ? AND P.idioma_id = ?';
			$params[] = $fonte;
		}

		$this->set('palavras', GenericDAO::selectQuery(
			$query,
			$params
		));
		if (Request::isAjax()) {
			$this->layout = 'json_response';
			$this->view = 'traduzir_ajax';
		}
	}
}
