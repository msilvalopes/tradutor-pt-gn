<?php
foreach (mb_list_encodings() as $key => $value) {
	#system("echo -e '".mb_convert_encoding( "$value \033[31mred\033[37m \\033[31mred\\033[37m\r\n",$value, 'UTF-8')."'");
}
#die;

function _echo($value) {
	global $echo_replaces;
	foreach (func_get_args() as $key => $value) {
		echo str_replace(array_keys($echo_replaces), array_values($echo_replaces), $value);
	}
}

function color($text = null, $color = 'light_gray') {
	$foreground = [
		'black' => '0;30',
		'dark_gray' => '1;30',
		'red' => '0;31',
		'bold_red' => '1;31',
		'green' => '0;32',
		'bold_green' => '1;32',
		'brown' => '0;33',
		'yellow' => '1;33',
		'blue' => '0;34',
		'bold_blue' => '1;34',
		'purple' => '0;35',
		'bold_purple' => '1;35',
		'cyan' => '0;36',
		'bold_cyan' => '1;36',
		'white' => '1;37',
		'bold_gray' => '0;37',
	];
	$background = [
		'black' => '40',
		'red' => '41',
		'magenta' => '45',
		'yellow' => '43',
		'green' => '42',
		'blue' => '44',
		'cyan' => '46',
		'light_gray' => '47',
	];
	$color_code = empty($background[$color]) ? $background['light_gray'] : $background[$color];
	return "\033[0;31m\033[43m\033[1m{$text}\033[0m";
}


if (date('Y-m-d') != '2016-01-29') {
	die('Fix date man!');
}
$palavras = json_decode(
	file_get_contents('data_new2.json'),
	true
);


global $classificacoes_palavras;
$classificacoes_palavras = json_decode(
	file_get_contents('./../migrations/seeds/classificacao_palavras.json'),
	true
);
global $echo_replaces;

$echo_replaces = [
	'ĩ' => color('~i', 'blue'),
	'Ĩ' => color('~I', 'blue'),
];

function get_classificacao_palavra_id($sigla) {
	global $classificacoes_palavras;
	ob_start();
	foreach ($classificacoes_palavras as $classificacao_palavra) {
		if ($classificacao_palavra['sigla'] == $sigla) {
			ob_get_clean();
			return $classificacao_palavra['id'];
		}
		_echo(" - {$classificacao_palavra['sigla']}\n");
	}
	_echo("classificacao_palavra [{$sigla}] não encontrada");
	die;
}
function clean_palavra($palavra) {
	#$nova_palavra = preg_replace('/$[-]+/', '', $palavra);
	#$nova_palavra = preg_replace('/[-]+^/', '', $palavra);
	$nova_palavra = trim($palavra);
	$aceitos = "/^[a-zA-Z'ãñĩ]/";
	$validos = preg_replace($aceitos, '', $palavra);
	if ($nova_palavra != $palavra) {
		_echo("$nova_palavra != $palavra");
		die;
	}
	return $nova_palavra;
}
function get_mode1 ($info, $all_data) {
	$dados = explode('.', $info);
	if (sizeof($dados) < 2) {
		return null;
	}
	$significado = explode(';', $dados[0]);
	if (sizeof($significado) < 2) {
		return null;
	}
	$palavras_em_pt = $significado[0];
	$palavras_em_pt = explode(',', $palavras_em_pt);
	foreach ($palavras_em_pt as &$palavra) {
		$palavra = trim($palavra);
		if (empty($palavra)) {
			unset($palavra);
		}
	}
	$palavras_em_pt = array_values($palavras_em_pt);
	unset($significado[0]);
	$significado = implode(';', $significado);
	unset($dados[0]);
	$dados = implode('.', $dados);
	return [
		'significado' => $significado,
		'explicacao' => $dados,
		'palavras_em_pt' => $palavras_em_pt,
	];
}

function get_mode2 ($info, $all_data) {
	$dados = explode('.', $info);
	if (sizeof($dados) < 2) {
		return null;
	}
	$significado = explode(':', $dados[0]);
	if (sizeof($significado) != 2) {
		var_dump($significado);
		return null;
	}
	$palavras_em_pt = $significado[1];
	$palavras_em_pt = explode(',', $palavras_em_pt);
	foreach ($palavras_em_pt as &$palavra) {
		$palavra = trim($palavra);
		if (empty($palavra)) {
			unset($palavra);
		}
	}
	$palavras_em_pt = array_values($palavras_em_pt);
	unset($significado[1]);
	$significado = implode(';', $significado);
	unset($dados[0]);
	$dados = implode('.', $dados);
	return [
		'significado' => $significado,
		'explicacao' => $dados,
		'palavras_em_pt' => $palavras_em_pt,
	];
}

function get_mode3 ($info, $all_data) {
	$tipos = ['part. nom.'];
	if (!in_array($all_data['type'], $tipos)) {
		return null;
	}
	$dados = explode('.', $info);
	if (sizeof($dados) < 2) {
		return null;
	}
	$significado = $dados[0];
	
	unset($dados[0]);
	return [
		'significado' => $significado,
		'explicacao' => implode('.', $dados),
		'palavras_em_pt' => '',
	];

}


function clean_palavra_pt($palavra) {
	_echo("PT-br: $palavra\nok (y/n)?");
	$line = fgets(STDIN);
	$line = preg_replace('/[\s\nn]/', '', $line);
	if ($line == "") {
		die('saindo PT');
	}

}
function normalize_info($info) {
	$params = [
		' 1a. ' => ' 1ª ',
		' 2a. ' => ' 2ª ',
		' 3a. ' => ' 3ª ',
	];
	return str_replace(array_keys($params), array_values($params), $info);
}
function split_info_into_all($info, $all_data) {
	echo "\n\n========================================================\n";
	$modes = [];
	$info = normalize_info($info);
	for ($i = 1; $i < 4; $i++) { 
		$func = "get_mode{$i}";
		
	}
	$dados_1 = print_r($mode_1 = get_mode1($info, $all_data), true);
	if ($mode_1) {
		$modes[] = 1;
		$dados_1 = implode("\n - ", explode("\n", $dados_1));
		_echo("mode 1:\n{$dados_1}\n");
	}
	$dados_2 = print_r($mode_2 = get_mode2($info, $all_data), true);
	if ($mode_2) {
		$modes[] = 2;
		$dados_2 = implode("\n - ", explode("\n", $dados_2));
		_echo("mode 2:\n{$dados_2}\n");
	}
	$dados_3 = print_r($mode_3 = get_mode3($info, $all_data), true);
	if ($mode_3) {
		$modes[] = 3;
		$dados_3 = implode("\n - ", explode("\n", $dados_3));
		_echo("mode 3:\n{$dados_3}\n");
	}
	ask:
	_echo("q to exit or 'i' to info or chose a mode: (".implode(',', $modes)."): ");
	$line = fgets(STDIN);
	$line = preg_replace('/[\s\n]/', '', $line);
	if ($line == "q") {
		die('saindo');
	}
	if ($line == 'iq' or $line == 'qi') {
		_echo($info, print_r($all_data, 1));
		die;
	}
	if ($line == 'i') {
		_echo($info, print_r($all_data, 1));
		goto ask;
	}
	if (!in_array($line, $modes)) {
		goto ask;
	}
	$return = "mode_{$line}";
	return $$return;	
}



$id = 1;

for ($key = 0; $key < sizeof($palavras); $key++) { 
	$palavra = $palavras[$key];
	$extras = split_info_into_all($palavra['info'], $palavra);

	$palavras_adicionadas[] = [
		'id' => $id,
		'idioma_id' => 2,
		'classificacao_palavra_id' => get_classificacao_palavra_id($palavra['type']),
		'palavra' => clean_palavra($palavra['word']),
		'significado' => $extras['significado'],
		'explicacao' => $extras['explicacao'],
		'criador_id' => 1,
		'ultimo_modificador_id' => 1,
		'data_criacao' => date('Y-m-d H:i:s'),
		'data_modificacao' => date('Y-m-d H:i:s'),
		'ativo' => 1,
		'validada' => 1,
	];
	$id++;
	foreach ($extras['palavras_em_pt'] as $palavra_em_pt) {
		$palavras_adicionadas[] = [
			'id' => $id,
			'idioma_id' => 1,
			'classificacao_palavra_id' => get_classificacao_palavra_id($palavra['type']),
			'palavra' => clean_palavra_pt($palavra_em_pt),
			'significado' => '?',
			'explicacao' => '?',
			'criador_id' => 1,
			'ultimo_modificador_id' => 1,
			'data_criacao' => date('Y-m-d H:i:s'),
			'data_modificacao' => date('Y-m-d H:i:s'),
			'ativo' => 1,
			'validada' => 0,
		];
		$id++;
	}

}

print_r($palavras[0]);