<?php
if (date('Y-m-d') != '2016-01-27') {
	die('Fix date man!');
}

function is_nextable_key($key, $next_word) {
	$dados = [
		'adv.' => ['modo'],
		'v.t.' => ['(irr)'],
		'v.i.' => ['(irr)'],
		'pref.' => ['relac.'],
		'part.' => ['col.'],
		'conj.' => ['subord.'],
		#'posp.' => ['de.'],
	];
	return (!empty($dados[$key])) and
	in_array($next_word, $dados[$key]);
}

function iskey($needle) {
	if (substr_count($needle, '.') < 1) {
		return false;
	}
	$dados = [
		'adj.', 'adv.', 'afirm.', 'art.',
		'asp.', 'conj.', 'coord.', 'dem.',
		'ênf.', 'enf.', 'est.', 'excl.',
		'i.', 'incl.', 'indef.', 'intens.',
		'interj.', 'interr.', 'irr.',
		'lit.', 'lug.', 'mod.', 'n.',
		'n.prop.', 'neg.', 'nom.', 'num.',
		'obj.', 'obs.', 'part.', 'pess.',
		'posp.', 'poss.', 'pref.', 'pron.',
		'sub.', 'suj.', 't.', 'temp.', 'v.',
	];
	if (in_array($needle, $dados)) {
		return true;
	}
	$words = explode('.', $needle);
	if ($words[sizeof($words) - 1] === '') {
		unset($words[sizeof($words) - 1]);
	}
	$valid = true;
	foreach ($words as $value) {
		$value = str_replace('/', '', $value);
		$valid = $valid && in_array("{$value}.", $dados);
	}
	return $valid;
}

function normalize($text) {
	$text = trim($text);
	$words = explode(' ', $text);

	// join ('C\d-' and 'Inf.\d+.\d+)'
	foreach ($words as $key => $value) {

		if (empty($words[$key + 1]) or empty($words[$key])) {
			continue;
		}
		if (!preg_match('/C\d[-]$/', $value)) {
			continue;
		}
		if (!preg_match('/^Inf./', $words[$key + 1])) {
			continue;
		}
		$words[$key] = $words[$key] . $words[$key + 1];
		unset($words[$key + 1]);
	}

	$word_base = true;
	foreach ($words as $key => $value) {

		if ($word_base and !iskey($value)) {
			$word[] = $value;

		} elseif (iskey($value)) {
			$type[] = $value;
			$word_base = false;
			$prev_word_is_key = true;

		} else {
			if ($prev_word_is_key) {
				$prev_word_is_key = false;
				if (is_nextable_key($words[$key - 1], $value)) {
					$type[] = $value;
					continue;
				}
			}
			$prev_word_is_key = false;

			$info[] = $value;
		}
	}
	return [
		'word' => implode(' ', $word),
		'type' => implode(' ', $type),
		'info' => implode(' ', $info),
	];
}
function debug() {
	static $debug;
	$args = func_get_args();
	if ($args[0] === 'on') {
		$debug = true;
		return;
	}
	if ($args[0] === 'off') {
		$debug = false;
		return;
	}
	if ($debug) {
		foreach ($args as $arg) {
			var_dump($arg);
		}
		readline(':');
	}
}

$file = explode("\n", file_get_contents('dados.txt'));

$prev = '';
$result = [];
foreach ($file as $key => $line) {
	$words = explode(' ', $line);
	$close = false;
	if (strpos($line, 'Arareiju') === 0) {
		#debug('on');
	}

	for ($i = 1; $i < min(4, sizeof($words)); $i++) {
		debug($words, $i, iskey($words[$i]));
		if (iskey($words[$i])) {
			$close = true;
			break;
		}
	}
	if ($close and $prev) {
		$result[] = normalize($prev);
		debug('off');
		$prev = '';
	}
	$prev = "{$prev} {$line}";
}
if ($prev) {
	$result[] = normalize($prev);
}
file_put_contents('data_new2.json', json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
echo sizeof($result);