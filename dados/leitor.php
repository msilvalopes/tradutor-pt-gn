<?php
if (date('Y-m-d') != '2016-01-27') {
	die('Fix date man!');
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

$file = explode("\n", file_get_contents('dados.txt'));

$prev = '';
$result = [];
// pre clean data
foreach ($file as $key => $line) {
	if (preg_match('/[0-9]{3}([\w]+\s?[\w]*)\s–\s\w+/', $line, $res)) {
		$new_line = substr($line, 3 + strlen($res[1]));
		if (strpos($new_line, $res[1]) === false) {
			var_dump('Fail on clean', $line, $res, $new_line);
		}
		$new_line = substr($new_line, strpos($new_line, $res[1]));
		$file[$key] = $new_line;
	}
}
foreach ($file as $key => $line) {
	$words = explode(' ', $line);
	if (sizeof($words) == 1) {
		var_dump('[one word on line]', $key, $words);
		die;
	}
	if (endsWith($words[1], '.')) {
		if (!empty($new_word)) {
			$result[] = $new_word;
		}
		$new_word = [
			'word' => $words[0],
			'type' => []
		];
		unset($words[0]);
		foreach ($words as $key => $value) {
			if (endsWith($words[$key], '.')) {
				$new_word['type'][] = $words[$key];
				unset($words[$key]);
				continue;
			}
			break;
		}
		$new_word['type'] = implode(' ', $new_word['type']);
		$new_word['info'] = implode(' ', $words);
	
	} elseif (!empty($words[2]) and endsWith($words[2], '.')) {

		if (!empty($new_word)) {
			$result[] = $new_word;
		}
		$new_word = [
			'word' => "$words[0] $words[1]",
			'type' => []
		];
		unset($words[0], $words[1]);
		foreach ($words as $key => $value) {
			if (endsWith($words[$key], '.')) {
				$new_word['type'][] = $words[$key];
				unset($words[$key]);
				continue;
			}
			break;
		}
		$new_word['type'] = implode(' ', $new_word['type']);
		$new_word['info'] = implode(' ', $words);
	
	} else {
		$new_word['info'] = $new_word['info'].' '.implode(' ', $words);		
	
	}
}
if (!empty($new_word)) {
	$result[] = $new_word;
}
file_put_contents('data.json', json_encode($result, JSON_PRETTY_PRINT| JSON_UNESCAPED_UNICODE));
echo sizeof($result);