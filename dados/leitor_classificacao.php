<?php
if (date('Y-m-d') != '2016-01-27') {
	die('Fix date man!');
}

$base = 'adj.
adv.
afirm.
art.
asp.
col.
conj.
coord.
dem.
enf.
ênf.
est.
excl.
i.
incl.
indef.
intens.
interj.
interr.
irr.
(irr)
lit.
lug.
mod.
modo
n.
n.prop.
neg.
nom.
num.
obj.
obs.
part.
pess.
posp.
poss.
pref.
pron.
prop.
relac.
sub.
subord.
suj.
t.
temp.
v.
/
adjetivo
advérbio
afirmação
artigo
aspectual
coletivo
conjunção
coordenativa
demonstrativo
ênfase
ênfase
estativo
exclusiva
intransitivo
inclusiva
indefinido
intensidade
interjeição
interrogativo(a)
irregular
irregular
literal
lugar
modo
modo
nome
nome próprio
negação
nominalizadora
numeral
objeto
observação
partícula
pessoal
posposição
possessivo
prefixo
pronome
próprio
relacional
subordinativa
subordinativa
sujeito
transitivo
temporal
verbo
/';

$dados = explode("\n", str_replace("\r", "\n",str_replace("\r\n", "\n", $base)));
while (sizeof($dados) > 0) {
	$titulos[$dados[0]] = $dados[intval(sizeof($dados) / 2)];
	unset($dados[intval(sizeof($dados) / 2)]);
	unset($dados[0]);	
	$dados = array_values($dados);
}
$data = json_decode(file_get_contents('data_new2.json'), true);
$_tipos = $tipos = [];
$_fix = ['v.t.' => 'v. t.'];
foreach ($data as &$value) {
	ob_start();
	$value['_type'] = $value['type'];
	if (!in_array($value['type'], $_tipos)) {
		$titulo = [];
		if (array_key_exists($value['type'], $_fix)) {
			$value['type'] = $_fix[$value['type']];
		}
		$value['type'] = str_replace(['.', '/'], ['. ', ' / '], $value['type']);
		$value['type'] = preg_replace('/\s+/', ' ', $value['type']);
		foreach(explode(" ", $value['type']) as $_title) {
			if (empty($_title)) {
				continue;
			}
			echo "{$_title} - ";
			if (empty($titulos[$_title])) {
				print_r($titulos);#, $_title);die;
				print_r($value);#, $_title);die;

				die("\n - {$value['type']} - ".utf8_decode($value['type'])." not found is [ {$_title} ]\n");
			}
			$titulo[] = $titulos[$_title];
		}
		echo '['.(implode(" ", $titulo)."] done\n");
		$_tipos[] = $value['_type'];
		$tipos[] = [
			'id' => sizeof($tipos) + 1,
			'sigla' => $value['_type'],
			'titulo' => trim(implode(" ", $titulo)),
			'explicacao' => '',
		];
	}
	ob_get_clean();
}
file_put_contents('classificacao_palavras.json', json_encode($tipos, JSON_PRETTY_PRINT| JSON_UNESCAPED_UNICODE));
echo "Imported: ".sizeof($tipos);
